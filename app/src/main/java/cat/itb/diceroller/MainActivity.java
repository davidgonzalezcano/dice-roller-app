package cat.itb.diceroller;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    ImageView title, dice1, dice2;
    Button rollButton, restart;
    TextView jackpot;
    private int num1, num2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rollButton = findViewById(R.id.roll_button);
        jackpot = findViewById(R.id.jackpot);
        restart = findViewById(R.id.restart);
        title = findViewById(R.id.title);
        dice1 = findViewById(R.id.dice1);
        dice2 = findViewById(R.id.dice2);

        //Clic al botó "ROLL THE DICE"
        rollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Ocultar "jackpot" al tornar a fer clic
                jackpot.setVisibility(View.INVISIBLE);

                // cambiar el text del botó a "DICE ROLLED"
                rollButton.setText(R.string.dice_Rolled);

                num1 = (int) (Math.random() * 6 + 1);
                num2 = (int) (Math.random() * 6 + 1);
                int d1 = 1;
                int d2 = 2;

                //Assignar imatge als daus
                rollTheDice(num1, d1);
                rollTheDice(num2, d2);

                //Comprobar si fa Jackpot
                jackpot(num1, num2);

                //Mostrar el botó de "RESTART"
                restart.setVisibility(View.VISIBLE);

                Toast.makeText(MainActivity.this, "He apretat el botó",
                        Toast.LENGTH_SHORT).show();
            }
        });

        //Clic al botó RESTART
        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dice1.setImageResource(R.drawable.empty_dice);
                dice2.setImageResource(R.drawable.empty_dice);
                rollButton.setText(R.string.roll_button);
                restart.setVisibility(View.INVISIBLE);
            }
        });

        //Clic Dau 1
        dice1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                num1 = (int) (Math.random() * 6 + 1);
                int dice = 1;

                rollTheDice(num1, dice);
                jackpot(num1, num2);
            }
        });

        //Clic Dau 2
        dice2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                num2 = (int) (Math.random() * 6 + 1);
                int dice = 2;

                rollTheDice(num2, dice);
                jackpot(num1, num2);
            }
        });
    }

    //Mètode per assignar les imatges dels daus
    public void rollTheDice(int num, int dice) {
        if (num == 1) {
            if (dice == 1) dice1.setImageResource(R.drawable.dice_1);
            if (dice == 2)  dice2.setImageResource(R.drawable.dice_1); }

        else if (num == 2) {
            if (dice == 1) dice1.setImageResource(R.drawable.dice_2);
            if (dice == 2)  dice2.setImageResource(R.drawable.dice_2); }

        else if (num == 3) {
            if (dice == 1) dice1.setImageResource(R.drawable.dice_3);
            if (dice == 2)  dice2.setImageResource(R.drawable.dice_3); }

        else if (num == 4) {
            if (dice == 1) dice1.setImageResource(R.drawable.dice_4);
            if (dice == 2)  dice2.setImageResource(R.drawable.dice_4); }

        else if (num == 5) {
            if (dice == 1) dice1.setImageResource(R.drawable.dice_5);
            if (dice == 2)  dice2.setImageResource(R.drawable.dice_5); }

        else {
            if (dice == 1) dice1.setImageResource(R.drawable.dice_6);
            if (dice == 2)  dice2.setImageResource(R.drawable.dice_6); }
    }

    //Mètode per a comprobar si es fa jackpot
    public void jackpot(int num1,int num2) {
        if(num1 == 6 && num2 == 6) jackpot.setVisibility(View.VISIBLE);
        else jackpot.setVisibility(View.INVISIBLE);
    }
}